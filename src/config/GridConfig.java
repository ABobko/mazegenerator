package config;

public class GridConfig {
    private int gridRows;
    private int gridColumns;
    private int cellSize;

    public GridConfig(int gridRows, int gridColumns, int cellSize){
        this.gridRows = gridRows;
        this.gridColumns = gridColumns;
        this.cellSize = cellSize;
    }

    public GridConfig(){
        this.gridRows = 20;
        this.gridColumns = 20;
        this.cellSize = 30;
    }

    public int getGridRows(){
        return gridRows;
    }
    public int getGridColumns(){
        return gridColumns;
    }
    public int getCellSize(){
        return cellSize;
    }
}
