package view;

import view.MazeDisplay;

import javax.swing.*;

public class PrimaryWindow extends JFrame {

    public PrimaryWindow(int width, int height) {
        MazeDisplay screen = new MazeDisplay(width, height);
        setTitle("MazeGenerator");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        add(screen);
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
    }
}