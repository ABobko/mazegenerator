package view;

import config.GridConfig;
import control.GridController;

import javax.swing.*;
import java.awt.*;


public class MazeDisplay extends JPanel {
    private GridController controller;

    MazeDisplay(int width, int height) {
        super();
        GridConfig gridConfig = new GridConfig(20, 20, 30);
        this.controller = new GridController();
        setPreferredSize(new Dimension(width, height));
        controller.generateGrid(gridConfig.getGridRows(),
                gridConfig.getGridColumns(),
                gridConfig.getCellSize());
        controller.generateMaze();
        repaint();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2));
        controller.drawGrid(g2);
    }
}
