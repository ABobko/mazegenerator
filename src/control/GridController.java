package control;

import model.Cell;

import java.awt.*;
import java.util.*;

public class GridController {

    private Cell[][] grid;
    private Cell selectedCell;
    private Stack<Cell> stack;
    private int rows;
    private int columns;

    public GridController(){
        this.stack= new Stack<>();
    }

    public Cell[][] generateGrid(int rows, int columns, int cellSize) {
        this.grid = new Cell[rows][columns];
        for (int j = 0; j < rows; j++) {
            for (int k = 0; k < columns; k++) {
                grid[j][k] = new Cell(j, k, cellSize);
            }
        }
        this.rows = rows;
        this.columns = columns;
        return grid;
    }

    //
    public void drawGrid(Graphics2D g2){
        for (Cell row[]: grid) {
            for (Cell elem : row) {
                drawCell(elem, g2);
            }
        }
    }

    private void drawCell(Cell c,Graphics2D g2) {
        LinkedList<Integer[]> existingWalls = c.getExistingWallsCoordinates();
        for (Integer[] wallCoordinates : existingWalls) {
            g2.drawLine(wallCoordinates[0],
                    wallCoordinates[1],
                    wallCoordinates[2],
                    wallCoordinates[3]);
        }
    }


    public void generateMaze(){
        selectedCell = grid[0][0];
        moveTo(selectedCell);
    }

    private void moveTo(Cell cell){
        selectedCell = cell;
        if(!selectedCell.wasVisited()){
            stack.push(selectedCell);
        }
        cell.setAsVisited();;
        Optional<Cell> randomUnvisitedCell = hasUnvisitedNeighbours(cell);
        if(randomUnvisitedCell.isPresent()){
            Cell nextCell = randomUnvisitedCell.get();
            removeWalls(cell, nextCell);
            moveTo(nextCell);
        } else if (!stack.isEmpty()){
            moveTo(selectedCell = stack.pop());
        }
    }

    private void removeWalls(Cell cell, Cell nextCell) {
        int column      = cell.getColumnIndex();
        int nextColumn  = nextCell.getColumnIndex();
        int row         = cell.getRowIndex();
        int nextRow     = nextCell.getRowIndex();

        if(column<nextColumn){
            cell.removeRightWall();
            nextCell.removeLeftWall();
        } else if(column>nextColumn){
            cell.removeLeftWall();
            nextCell.removeRightWall();
        } else if(row<nextRow){
            cell.removeBottomWall();
            nextCell.removeTopWall();
        } else if(row>nextRow){
            cell.removeTopWall();
            nextCell.removeBottomWall();
        }
    }

    private Optional<Cell> hasUnvisitedNeighbours(Cell cell){
        int row = cell.getRowIndex();
        int column = cell.getColumnIndex();
        ArrayList<Cell> unvisitedCells= new ArrayList<>();
        //check if upper cell was visited
        if(row>0&&!grid[row-1][column].wasVisited()){
            unvisitedCells.add(grid[row-1][column]);
        }
        //check if bottom cell was visited
        if(row+1<this.rows&&!grid[row+1][column].wasVisited()){
            unvisitedCells.add(grid[row+1][column]);
        }
        //check if cell to the left was visited
        if(column>0&&!grid[row][column-1].wasVisited()){
            unvisitedCells.add(grid[row][column-1]);
        }
        //check if cell to the bottom was visited
        if(column+1<this.columns&&!grid[row][column+1].wasVisited()){
            unvisitedCells.add(grid[row][column+1]);
        }
        if(!unvisitedCells.isEmpty()){
            Random random = new Random();
            int index = random.nextInt(unvisitedCells.size());
            return Optional.of(unvisitedCells.get(index));
        }
        return Optional.empty();

    }

}
