package model;

import java.util.Collections;
import java.util.LinkedList;

public class Cell{

    private int size;

    private int rowIndex;
    private int columnIndex;
    private boolean hasTopWall;
    private boolean hasRightWall;
    private boolean hasLeftWall;
    private boolean hasBottomWall;
    private boolean visited;

    public Cell(int rowIndex, int columnIndex, int cellSize){
        this.visited = false;
        this.size    = cellSize;
        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;
        this.hasTopWall    = true;
        this.hasRightWall  = true;
        this.hasLeftWall   = true;
        this.hasBottomWall = true;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public boolean wasVisited() {
        return visited;
    }

    public void setAsVisited() {
        this.visited = true;
    }

    private int getTopLeftX(){
        return columnIndex*size;
    }
    private int getTopLeftY(){
        return rowIndex*size;
    }
    private int getTopRightX(){
        return columnIndex*size +size;
    }
    private int getTopRightY(){
        return rowIndex*size;
    }
    private int getBottomRightX(){
        return columnIndex*size +size;
    }
    private int getBottomRightY(){
        return rowIndex*size +size;
    }
    private int getBottomLeftX(){
        return columnIndex*size;
    }
    private int getBottomLeftY(){
        return rowIndex*size +size;
    }

    public LinkedList<Integer[]> getExistingWallsCoordinates(){
        LinkedList<Integer[]> existingWalls = new LinkedList<>();
        if(hasTopWall){
            existingWalls.add(new Integer[]{getTopLeftX(), getTopLeftY(),getTopRightX(),getTopRightY()});
        }
        if(hasRightWall){
            existingWalls.add(new Integer[]{getTopRightX(),getTopRightY(),getBottomRightX(),getBottomRightY()});
        }
        if(hasBottomWall){
            existingWalls.add(new Integer[]{getBottomRightX(),getBottomRightY(),getBottomLeftX(),getBottomLeftY()});
        }
        if(hasLeftWall){
            existingWalls.add(new Integer[]{getBottomLeftX(),getBottomLeftY(),getTopLeftX(),getTopLeftY()});
        }
        return existingWalls;
    }

    public void removeTopWall(){
        hasTopWall = false;
    }
    public void removeRightWall(){
        hasRightWall = false;
    }
    public void removeBottomWall(){
        hasBottomWall = false;
    }
    public void removeLeftWall(){
        hasLeftWall = false;
    }
}
